
package cl.aiep.venta.model;


import java.sql.Date;

public class IngresoV {
    
    String folio;
    Date fecha;
    String tipo;
    String nombre;

    public IngresoV(String folio, Date fecha, String tipo, String nombre) {
        this.folio = folio;
        this.fecha = fecha;
        this.tipo = tipo;
        this.nombre = nombre;
      
    }

    public IngresoV(String folio, String tipo, Date fecha, String nombre) {
           this.folio = folio;
        this.tipo = tipo;
        this.fecha = fecha;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
    
    
    
    
}
