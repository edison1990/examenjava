package cl.aiep.venta.conexion;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Conexion {
    
  private String server;
    private String user;
    private String base;
    private String clave;
    private int port;
    private String url;
   
    private Connection conexion;
    private Statement st;
    private ResultSet rs;

    public Conexion() {
        this.clave  = "edison1990s";
        this.server = "localhost";
        this.user   = "root";
        this.port   = 3306;
        this.base   = "DONA_JUANITA";
   
         this.url ="jdbc:mysql://"+this.server+":"+this.port+"/"+this.base+"?characterEncoding=latin1";
    }
        public void getConnection() throws SQLException, InstantiationException, IllegalAccessException{
        this.conexion = null;
       try{
        
        Class.forName("com.mysql.jdbc.Driver");


           this.conexion = DriverManager.getConnection(  
                   this.url,  
                   this.user,
                   this.clave
           );
         
           System.out.println(" exito al conectarse");
       }catch(ClassNotFoundException ex){
          
            System.out.println("Error de conexion : " + ex.getMessage());
       }
       
       
       
    }
     
    
        public void ejecutar(String query) throws SQLException{
        
            st = conexion.createStatement();
            st.executeUpdate(query);
            st.close();
          
        }
        
        public ResultSet ejecutarSelect(String query) throws SQLException {
        
            st=conexion.createStatement();
            rs=st.executeQuery(query);
            return rs;
            
        
        }
        
        public void desconectar() throws SQLException{
        
            st.close();
        
        }
    
}
