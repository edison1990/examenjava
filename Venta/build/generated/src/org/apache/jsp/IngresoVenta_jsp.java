package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import cl.aiep.venta.conexion.Conexion;
import java.sql.ResultSet;

public final class IngresoVenta_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
      out.write('\n');

    Conexion cnx = new Conexion();
    cnx.getConnection();


      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Ingreso Venta</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("         <div class=\"container-form\">\n");
      out.write("             \n");
      out.write("                <h2>Ingreso De Ventas</h2>\n");
      out.write("                \n");
      out.write("                     <form action=\"ServIngresoVenta\" method=\"POST\">\n");
      out.write("              \n");
      out.write("                         <span class=\"label\"> Folio: </span>\n");
      out.write("                            <input type=\"text\" name=\"folio\"> \n");
      out.write("                        \n");
      out.write("                    &nbsp; &nbsp; &nbsp;\n");
      out.write("                  \n");
      out.write("                              ");



ResultSet result2 =  cnx.ejecutarSelect("select Id,Tipo From TipoVenta");

 
      out.write("\n");
      out.write("        <span>Tipo de Venta: </span>\n");
      out.write("        <select class=\"select\" name=\"tipoventa\">\n");
      out.write("            \n");
      out.write("              <option value=\"0\">Seleccione</option>\n");
      out.write(" \n");
      out.write("                    ");

                        while( result2.next()){
                            out.println("<option value='"+result2.getInt(1)+"'>");
                                out.println(result2.getString(2));
                            out.println("</option>");
                        } 
                    
      out.write("\n");
      out.write("    \n");
      out.write("                           \n");
      out.write("                         &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;\n");
      out.write("                            \n");
      out.write("                         <span class=\"label\"> fecha Venta</span>\n");
      out.write("                           <input id=\"date\" type=\"date\"> \n");
      out.write("                            \n");
      out.write("                            <br><br>\n");
      out.write("                        \n");
      out.write("                     \n");
      out.write("                               ");



ResultSet result1 =  cnx.ejecutarSelect("select CliRut, CONCAT(CliNmbre, ' ', CliApellido) As Nombre From Cliente");

 
      out.write("\n");
      out.write("        <span>Nombre Cliente: </span>\n");
      out.write("        <select class=\"select\" name=\"nombre\">\n");
      out.write("            \n");
      out.write("              <option value=\"0\">Seleccione</option>\n");
      out.write(" \n");
      out.write("                    ");

                        while( result1.next()){
                            out.println("<option value='"+result1.getInt(1)+"'>");
                                out.println(result1.getString(2));
                            out.println("</option>");
                        } 
                    
      out.write("\n");
      out.write("        </select> &nbsp; &nbsp; &nbsp;\n");
      out.write("              \n");
      out.write(" \n");
      out.write("                  <input type=\"submit\" name=\"boton\" value=\"Crear venta\">\n");
      out.write("       \n");
      out.write("                      </form> \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("          <table class=\"tabla1\" border=\"1\">\n");
      out.write("            <tr>\n");
      out.write("                <th>Folio</th>\n");
      out.write("                <th>Tipo de venta</th>\n");
      out.write("                <th>Fecha</th> \n");
      out.write("                <th>Nombre Cliente</th> \n");
      out.write("            </tr>\n");
      out.write("            ");

              
             
              String query = "select * from VW_TAB1";
              ResultSet  lista = cnx.ejecutarSelect(query);
               
                while( lista.next()){
                    out.println("<tr>");
                        out.println("<td>" + lista.getObject("Folio")+ "</td>");
                        out.println("<td>" + lista.getObject("TipodeVenta")+ "</td>");
                        out.println("<td>" + lista.getObject("Fecha")+ "</td>"); 
                        out.println("<td>" + lista.getObject("NombreCliente")+ "</td>"); 
                    out.println("</tr>");  
                }
                
            
      out.write("\n");
      out.write("            \n");
      out.write("        </table>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("                     </div>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("    </body>\n");
      out.write("       \n");
      out.write("                      \n");
      out.write("    </html>                      \n");
      out.write("                  \n");
      out.write("                      \n");
      out.write("                       \n");
      out.write("                        \n");
      out.write("                 \n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
