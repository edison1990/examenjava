CREATE DATABASE DONA_JUANITA

USE DONA_JUANITA

CREATE TABLE Usuario(
Id INT PRIMARY KEY AUTO_INCREMENT,
Nombre VARCHAR(50),
Correo VARCHAR(60),
Clave VARCHAR(20)

);

INSERT INTO Usuario(Nombre, Correo, Clave) VALUES ('Edison', 'edison@mail.com', 'edi1');
INSERT INTO Usuario(Nombre, Correo, Clave) VALUES ('Juan', 'juan@mail.com', 'juan1');
INSERT INTO Usuario(Nombre, Correo, Clave) VALUES ('Oscar', 'oscar@mail.com', 'osc1');


drop TABLE Cliente (

CliRut INT PRIMARY KEY,
CliNmbre VARCHAR(50),
CliApellido VARCHAR(50),
CliFechaIngreso DATE,
CliDireccion VARCHAR(50),
CliFono INT
);

INSERT INTO Cliente(CliRut, CliNmbre, CliApellido, CliFechaIngreso, CliDireccion, CliFono  ) VALUES (16543234, 'jose', 'Pardo', '12-04-18', 'carreras #258', 12345678);



create TABLE TipoVenta(
Id int primary key auto_increment,
Tipo varchar(30)

);

insert into TipoVenta (Tipo) values ('Boleta');
insert into TipoVenta (Tipo) values ('Factura');






create TABLE venta (
Id INT PRIMARY KEY AUTO_INCREMENT,
VenFolio INT,
VenFecha DATE,
IdTipo INT,
FOREIGN KEY(IdTipo) REFERENCES TipoVenta(Id),
CliRut INT,
FOREIGN KEY(CliRut) REFERENCES Cliente(CliRut)

);






select * From Venta;





CREATE TABLE Producto(

ProCodigo INT PRIMARY KEY,
ProCodigobarra INT,
ProNombre VARCHAR(50),
ProMarca VARCHAR(50),
proPrecio INT

);






create TABLE Detalle_Venta(

Id INT PRIMARY KEY AUTO_INCREMENT,
IdVenta INT,
FOREIGN KEY(IdVenta) REFERENCES Venta(Id),
ProCodigo INT,
FOREIGN KEY(ProCodigo) REFERENCES Producto(ProCodigo),
VenCantidad INT,
VenPrecioReal INT,
VenDescuento INT,
VenPrecioVenta INT
);

select * from venta;



create VIEW VW_TAB1
as
select  venFolio as Folio, TipoVenta.Tipo as TipodeVenta, VenFecha as Fecha, Cliente.CliNmbre as NombreCliente  From Venta
INNER JOIN TipoVenta ON Venta.IdTipo = TipoVenta.Id 
INNER JOIN Cliente ON Venta.CliRut = Cliente.CliRut;

select * from VW_TAB1




